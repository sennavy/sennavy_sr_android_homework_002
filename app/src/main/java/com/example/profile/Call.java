package com.example.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Call extends AppCompatActivity {
    Button btnCall;
    EditText txtPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        txtPhone = findViewById(R.id.txtPhone);

        btnCall = findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("PHONE", "phone is " + txtPhone.getText().toString());
                Call();
            }
        });
    }
    public void Call()
    {
        String phone = txtPhone.getText().toString();
        if (!txtPhone.getText().toString().isEmpty()){
            Intent intent = new Intent();
            Uri uri = Uri.parse("tel:" + phone);
            intent.setAction(Intent.ACTION_DIAL);
            intent.setData(uri);
            startActivity(intent);
        }
        else{
            Toast.makeText(this, "Invalid input", Toast.LENGTH_LONG).show();
        }
    }
}
