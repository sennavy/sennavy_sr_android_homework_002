package com.example.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Fillnote extends AppCompatActivity {
    Button btnSaveNote;
    Button btnCancelNote;
    EditText title;
    EditText content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fillnote);

        btnSaveNote = findViewById(R.id.btnSaveNote);
        btnCancelNote = findViewById(R.id.btnCancelNote);
        title = findViewById(R.id.txtTitle);
        content = findViewById(R.id.txtContent);

        btnSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToNote();
            }
        });
        btnCancelNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelSaveNote();
            }
        });
    }
    public void backToNote(){
        String stringTitle = title.getText().toString();
        String stringContent = content.getText().toString();

        if(stringTitle.isEmpty()){
            Toast.makeText(this, "Please input title", Toast.LENGTH_SHORT).show();
        }
        else if( stringContent.isEmpty()){
            Toast.makeText(this, "Please input content", Toast.LENGTH_SHORT).show();
        }
        else {
            Intent intent = new Intent();
            intent.putExtra("Title", title.getText().toString());
            intent.putExtra("Content", content.getText().toString());
            setResult(RESULT_OK,intent);
            finish();
        }
    }
    public void cancelSaveNote(){
        finish();
    }
}
