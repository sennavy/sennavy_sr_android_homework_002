package com.example.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {
    FrameLayout btnGallery;
    FrameLayout btnContact;
    FrameLayout btnCall;
    FrameLayout btnNotebook;
    FrameLayout btnProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnGallery = findViewById(R.id.btnGallery);
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        btnContact = findViewById(R.id.btnContact);
        btnContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContact();
            }
        });

        btnCall = findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCall();
            }
        });

        btnNotebook = findViewById(R.id.btnNotebook);
        btnNotebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNotebook();
            }
        });

        btnProfile = findViewById(R.id.btnProfile);
        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openProfile();
            }
        });
    }
    private void openGallery(){
        Intent intent = new Intent(this,Gallery.class);
        startActivity(intent);
    }
    private void openContact(){
        Intent intent = new Intent(this,Contact.class);
        startActivity(intent);
    }
    private void openCall(){
        Intent intent = new Intent(this,Call.class);
        startActivity(intent);
    }
    private void openNotebook(){
        Intent intent = new Intent(this, Notebook.class);
        startActivity(intent);
    }
    private void openProfile(){
        Intent intent = new Intent(this, Profile.class);
        startActivity(intent);
    }
}
