package com.example.profile;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Notebook extends AppCompatActivity {
    LinearLayout btnAddNote;
    ImageButton btnAdd;
    private static int REQUEST_CODE = 1;
    GridLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notebook);

        btnAdd = findViewById(R.id.btnAdd);
        rootLayout = findViewById(R.id.root_layout);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddNote();
            }
        });
    }

    public void openAddNote(){
        Intent intent = new Intent(this,Fillnote.class);
        startActivityForResult(intent,REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE && resultCode == RESULT_OK ){
            LinearLayout linearLayout = new LinearLayout(this);
            GridLayout.LayoutParams params = new GridLayout.LayoutParams();
            params.rowSpec = GridLayout.spec(GridLayout.UNDEFINED, 2);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                params.columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);
            }

            params.width = 300;
            params.height = 208;
            linearLayout.setLayoutParams(params);
            linearLayout.setBackgroundColor(getResources().getColor(R.color.White));
            linearLayout.setOrientation(LinearLayout.VERTICAL);

            TextView txtTitle = new TextView(this);
            txtTitle.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
            txtTitle.setEllipsize(TextUtils.TruncateAt.END);
            txtTitle.setLines(1);
            txtTitle.setTextSize(35);
            txtTitle.setPadding(20,10,15,10);
            txtTitle.setTypeface(null, Typeface.BOLD);

            TextView txtContent = new TextView(this);
            txtContent.setEllipsize(TextUtils.TruncateAt.END);
            txtContent.setLines(2);
            txtContent.setTextSize(15);
            txtContent.setPadding(20,10,15,10);

            linearLayout.addView(txtTitle);
            linearLayout.addView(txtContent);

            txtTitle.setText(data.getStringExtra("Title"));
            txtContent.setText(data.getStringExtra("Content"));
            rootLayout.addView(linearLayout);
        }
    }
}
